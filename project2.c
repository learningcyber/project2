#include <stdio.h>
#include <dlfcn.h>

#ifdef __APPLE__
#define SHARED_EXT ".so"
#elif
#define SHARED_EXT ".dll"
#else
#define SHARED_EXT ".so"
#endif

typedef int (*mediaConnectFnc)(void);

int main()
{
    int success = 0;
    printf("Attempting to load MediaHub"SHARED_EXT"\n");

    void* mediaHubSO = dlopen("MediaHub"SHARED_EXT, RTLD_NOW); 

    if(mediaHubSO)
    {
        printf("Attempting to find `mediaConnect` function...\n");

        mediaConnectFnc fn = dlsym(mediaHubSO, "mediaConnect");

        if(fn)
        {
            success = (*fn)();
        }
        else
        {
            printf("Unable to find `mediaConnect` function");
        }

        dlclose(mediaHubSO);
    }
    else
    {
        printf("Unable to load MediaHub"SHARED_EXT"\n");
    }

    if(success)
    {
        printf("Media connection successful!!! Operations files will be sent to you shortly...\n");
    }
    else
    {
        printf("Unable to connect. Please wait to get to the operations center before attempting to connect to the media server!\n");
    }

    return 0;
}
